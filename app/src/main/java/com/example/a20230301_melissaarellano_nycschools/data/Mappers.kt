package com.example.a20230301_melissaarellano_nycschools.data

import com.example.a20230301_melissaarellano_nycschools.data.network.SchoolItemResponse
import com.example.a20230301_melissaarellano_nycschools.data.network.SchoolScoresResponse
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.models.SchoolScores

fun SchoolItemResponse.toSchool() = School(
    dbn = dbn,
    name = schoolName,
    location = "$stateCode, $city",
    overview = overviewParagraph,
    phone = phoneNumber,
    email = schoolEmail,
    website = website
)

fun SchoolScoresResponse.toSchoolScores() = SchoolScores(
    testTaken = numOfSatTestTakers.toIntOrNull() ?: 0,
    mathScore = satMathAvgScore.toIntOrNull() ?: 0,
    readingScore = satCriticalReadingAvgScore.toIntOrNull() ?: 0,
    writingScore = satWritingAvgScore.toIntOrNull() ?: 0
)

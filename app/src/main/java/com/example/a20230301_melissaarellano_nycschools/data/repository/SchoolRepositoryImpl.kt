package com.example.a20230301_melissaarellano_nycschools.data.repository

import com.example.a20230301_melissaarellano_nycschools.data.network.SchoolsApi
import com.example.a20230301_melissaarellano_nycschools.data.toSchool
import com.example.a20230301_melissaarellano_nycschools.data.toSchoolScores
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.models.SchoolScores
import com.example.a20230301_melissaarellano_nycschools.domain.repository.SchoolRepository
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(
    private val api: SchoolsApi
) : SchoolRepository {
    override suspend fun getAllSchools(): List<School> {
        val response = api.fetchAllSchools()

        return when (response.isSuccessful) {
            true -> {
                response.body()?.map { it.toSchool() } ?: emptyList()
            }
            false -> {
                emptyList()
            }
        }

    }

    override suspend fun getSchoolScores(schoolDbn: String): SchoolScores? {
        val response = api.fetchSchoolScores(schoolDbn)
        return when (response.isSuccessful) {
            true -> {
                response.body()?.firstOrNull()?.toSchoolScores()
            }
            false -> {
                null
            }
        }
    }
}
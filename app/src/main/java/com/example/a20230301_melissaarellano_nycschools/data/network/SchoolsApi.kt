package com.example.a20230301_melissaarellano_nycschools.data.network

import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {

    @GET("resource/s3k6-pzi2.json")
    suspend fun fetchAllSchools(): Response<List<SchoolItemResponse>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun fetchSchoolScores(
        @Query("dbn") dbn: String
    ): Response<List<SchoolScoresResponse>>
}
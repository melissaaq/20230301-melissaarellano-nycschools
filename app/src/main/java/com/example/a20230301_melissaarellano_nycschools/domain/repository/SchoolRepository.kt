package com.example.a20230301_melissaarellano_nycschools.domain.repository

import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.models.SchoolScores

interface SchoolRepository {
    suspend fun getAllSchools(): List<School>
    suspend fun getSchoolScores(schoolDbn: String): SchoolScores?
}
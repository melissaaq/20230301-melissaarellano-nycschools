package com.example.a20230301_melissaarellano_nycschools.di

import com.example.a20230301_melissaarellano_nycschools.data.repository.SchoolRepositoryImpl
import com.example.a20230301_melissaarellano_nycschools.domain.repository.SchoolRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun providesSchoolRepository(schoolRepositoryImpl: SchoolRepositoryImpl) : SchoolRepository
}
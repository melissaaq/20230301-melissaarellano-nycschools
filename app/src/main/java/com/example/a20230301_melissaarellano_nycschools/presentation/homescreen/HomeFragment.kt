package com.example.a20230301_melissaarellano_nycschools.presentation.homescreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.findNavController
import com.example.a20230301_melissaarellano_nycschools.R
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.presentation.ui.theme.A20230301MelissaArellanoNYCSchoolsTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val viewModel by viewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                A20230301MelissaArellanoNYCSchoolsTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colors.background
                    ) {
                        MainScreen(viewModel)
                    }
                }

                viewModel.detailsNavigate.observe(viewLifecycleOwner) { school ->
                    findNavController().navigate(
                        HomeFragmentDirections.actionHomeFragmentToDetailsFragment(
                            school
                        )
                    )
                }
            }
        }
    }

    @Preview(showBackground = true, showSystemUi = true)
    @Composable
    fun MainScreen(
        viewModel: HomeViewModel = viewModel()
    ) {
        val schoolList by viewModel.schoolList.observeAsState()
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            SchoolList(
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .fillMaxWidth()
                    .fillMaxHeight(),
                list = schoolList ?: emptyList(),
                onSchoolClick = { viewModel.onSchoolClicked(it) }
            )
        }
    }

    @Composable
    fun SchoolList(
        modifier: Modifier,
        list: List<School>,
        onSchoolClick: (School) -> Unit = {}
    ) {
        LazyColumn(modifier = modifier) {
            items(
                items = list
            ) { school ->
                SchoolListItem(school = school, onClick = onSchoolClick)
            }
        }
    }

    @Composable
    fun SchoolListItem(
        modifier: Modifier = Modifier,
        school: School,
        onClick: (School) -> Unit = {}
    ) {
        Card(
            modifier = Modifier.padding(top = 6.dp).fillMaxWidth(),
            elevation = 10.dp
        ) {
            Column(
                modifier = modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Text(
                    text = school.dbn,
                    fontSize = 16.sp,
                    color = Color.Black
                )
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(0.7f),
                        text = school.name,
                        fontSize = 16.sp,
                        color = Color.Black,
                        fontWeight = FontWeight.Bold
                    )
                    Button(onClick = { onClick(school)  }) {
                        Text(text = stringResource(R.string.details))
                    }
                }
                Row(Modifier.fillMaxWidth()) {
                    Icon(
                        imageVector = Icons.Rounded.LocationOn,
                        contentDescription = null,
                        Modifier.padding(end = 8.dp)
                    )
                    Text(
                        text = school.location,
                        fontSize = 16.sp,
                        color = Color.Black
                    )
                }
            }
        }
    }
}
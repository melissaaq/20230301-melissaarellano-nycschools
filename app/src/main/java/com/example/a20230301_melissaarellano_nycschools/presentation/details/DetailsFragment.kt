package com.example.a20230301_melissaarellano_nycschools.presentation.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.a20230301_melissaarellano_nycschools.R
import com.example.a20230301_melissaarellano_nycschools.databinding.SchoolDetailsScreenBinding
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.models.SchoolScores
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment() {
    private val viewModel by viewModels<DetailsViewModel>()
    private lateinit var binding: SchoolDetailsScreenBinding
    private val args by navArgs<DetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SchoolDetailsScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showSchool(args.schoolSelected)
        viewModel.loadSatScores(args.schoolSelected)
        viewModel.schoolScores.observe(viewLifecycleOwner) { scores ->
            showScores(scores)
        }
    }

    private fun showSchool(school: School) {
        with(binding) {
            (requireActivity() as? AppCompatActivity)?.supportActionBar?.title = school.dbn
            detailsName.text = school.name
            detailsEmail.text = school.email
            detailsLocation.text = school.location
            detailsPhone.text = school.phone
            detailsWeb.text = school.website
            detailsParagraph.text = school.overview
        }
    }

    private fun showScores(scores: SchoolScores?) {
        with(binding) {
            if (scores == null) {
                detailsMathScore.text = getString(R.string.school_details_no_score)
                detailsReadingScore.text = getString(R.string.school_details_no_score)
                detailsWritingScore.text = getString(R.string.school_details_no_score)
            } else {
                detailsMathScore.text =
                    getString(R.string.school_details_math_format, scores.mathScore)
                detailsReadingScore.text =
                    getString(R.string.school_details_reading_format, scores.readingScore)
                detailsWritingScore.text =
                    getString(R.string.school_details_writing_format, scores.writingScore)
            }
        }
    }
}
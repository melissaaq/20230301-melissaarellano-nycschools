package com.example.a20230301_melissaarellano_nycschools.presentation.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.models.SchoolScores
import com.example.a20230301_melissaarellano_nycschools.domain.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val repository: SchoolRepository
): ViewModel() {

    private val _schoolScores = MutableLiveData<SchoolScores?>()
    val schoolScores: LiveData<SchoolScores?> get() = _schoolScores

    fun loadSatScores(schoolSelected: School) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repository.getSchoolScores(schoolSelected.dbn)
            _schoolScores.postValue(result)
        }
    }

}
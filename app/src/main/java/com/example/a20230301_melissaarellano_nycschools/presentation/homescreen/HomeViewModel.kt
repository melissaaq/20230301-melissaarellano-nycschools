package com.example.a20230301_melissaarellano_nycschools.presentation.homescreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.repository.SchoolRepository
import com.hadilq.liveevent.LiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: SchoolRepository
) : ViewModel() {

    private val _schoolList = MutableLiveData<List<School>>()
    val schoolList: LiveData<List<School>>
        get() = _schoolList

    private val _detailsNavigate = LiveEvent<School>()
    val detailsNavigate: LiveData<School> get() = _detailsNavigate


    init {
        loadSchools()
    }

    fun loadSchools() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repository.getAllSchools()
            _schoolList.postValue(result)
        }
    }

    fun onSchoolClicked(school: School) {
        _detailsNavigate.postValue(school)
    }
}
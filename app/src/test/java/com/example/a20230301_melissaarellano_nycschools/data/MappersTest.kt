package com.example.a20230301_melissaarellano_nycschools.data

import com.example.a20230301_melissaarellano_nycschools.data.network.SchoolItemResponse
import com.example.a20230301_melissaarellano_nycschools.data.network.SchoolScoresResponse
import com.google.common.truth.Truth.assertThat
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Before
import org.junit.Test

class MappersTest {

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `SchoolItemResponse to School maps correctly`() {
        //GIVEN
        val schoolItemResponse = SchoolItemResponse(
            city = "Atlanta",
            dbn = "1234",
            latitude = "1111",
            longitude = "2222",
            overviewParagraph = "Test description paragraph",
            phoneNumber = "1234567890",
            primaryAddressLine1 = "Test primary address line 1",
            schoolEmail = "school@test.com",
            schoolName = "School Test",
            stateCode = "GA",
            totalStudents = "39",
            website = "www.test.com",
            zip = "12345"
        )

        //WHEN
        val school = schoolItemResponse.toSchool()

        //THEN
        assertThat(school.dbn).isEqualTo("1234")
        assertThat(school.name).isEqualTo("School Test")
        assertThat(school.location).isEqualTo("GA, Atlanta")
        assertThat(school.website).isEqualTo("www.test.com")
        assertThat(school.email).isEqualTo("school@test.com")
        assertThat(school.overview).isEqualTo("Test description paragraph")
        assertThat(school.phone).isEqualTo("1234567890")
    }

    @Test
    fun `SchoolScoresResponse to SchoolScores maps correctly`() {
        //GIVEN
        val scoreResponse = SchoolScoresResponse(
            dbn = "1234",
            numOfSatTestTakers = "4",
            satCriticalReadingAvgScore = "11",
            satMathAvgScore = "12",
            satWritingAvgScore = "13",
            schoolName = "School Test"
        )

        //WHEN
        val schoolScore = scoreResponse.toSchoolScores()

        //THEN
        assertThat(schoolScore.mathScore).isEqualTo(12)
        assertThat(schoolScore.readingScore).isEqualTo(11)
        assertThat(schoolScore.testTaken).isEqualTo(4)
        assertThat(schoolScore.writingScore).isEqualTo(13)
    }
}
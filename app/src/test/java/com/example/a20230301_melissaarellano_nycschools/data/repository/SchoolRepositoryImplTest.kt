package com.example.a20230301_melissaarellano_nycschools.data.repository

import com.example.a20230301_melissaarellano_nycschools.data.network.SchoolItemResponse
import com.example.a20230301_melissaarellano_nycschools.data.network.SchoolsApi
import com.example.a20230301_melissaarellano_nycschools.domain.repository.SchoolRepository
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Response

internal class SchoolRepositoryImplTest {

    private val api: SchoolsApi = mockk(relaxed = true)
    private val repository: SchoolRepository = SchoolRepositoryImpl(api)
    private val schoolsResponseMock = listOf<SchoolItemResponse>(
        mockk(relaxed = true),
        mockk(relaxed = true),
        mockk(relaxed = true)
    )

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `getAllSchools() is successful then returns list of school`() = runTest {
        //GIVEN
        val response: Response<List<SchoolItemResponse>> = mockk(relaxed = false)
        every { response.isSuccessful } returns true
        every { response.body() } returns schoolsResponseMock
        coEvery {
            api.fetchAllSchools()
        } returns response

        //WHEN
        val result = repository.getAllSchools()

        //THEN
        assertThat(result).hasSize(3)
    }

    @Test
    fun `getAllSchools() is failure then returns empty list`() = runTest {
        //GIVEN
        val response: Response<List<SchoolItemResponse>> = mockk(relaxed = false)
        every { response.isSuccessful } returns false
        coEvery {
            api.fetchAllSchools()
        } returns response

        //WHEN
        val result = repository.getAllSchools()

        //THEN
        assertThat(result).isEmpty()
    }
}
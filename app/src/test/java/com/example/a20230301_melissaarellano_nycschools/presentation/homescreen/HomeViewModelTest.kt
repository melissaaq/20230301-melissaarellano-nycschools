package com.example.a20230301_melissaarellano_nycschools.presentation.homescreen

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.repository.SchoolRepository
import com.example.a20230301_melissaarellano_nycschools.getOrAwaitValue
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HomeViewModelTest {
    private val repository: SchoolRepository = mockk(relaxed = false)
    private val viewModel: HomeViewModel = HomeViewModel(repository)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        coEvery { repository.getAllSchools() } returns emptyList()
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `When loadSchools returns schools then schoolList is posted`() {
        val school: School = mockk(relaxed = true)
        val schoolList: List<School> = listOf(
            school,
            school,
            school
        )
        //GIVEN
        coEvery { repository.getAllSchools() } returns schoolList

        //WHEN
        viewModel.loadSchools()
        val state = viewModel.schoolList.getOrAwaitValue()

        //THEN
        Truth.assertThat(state).isNotNull()
        Truth.assertThat(state).hasSize(3)
    }

    @Test
    fun `When loadSchools returns empty then schoolList is posted`() {
        val schoolList: List<School> = emptyList()
        //GIVEN
        coEvery { repository.getAllSchools() } returns schoolList

        //WHEN
        viewModel.loadSchools()
        val state = viewModel.schoolList.getOrAwaitValue()

        //THEN
        Truth.assertThat(state).isNotNull()
        Truth.assertThat(state).isEmpty()
    }
}
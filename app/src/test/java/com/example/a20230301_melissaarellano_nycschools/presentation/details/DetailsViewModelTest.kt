package com.example.a20230301_melissaarellano_nycschools.presentation.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230301_melissaarellano_nycschools.domain.models.School
import com.example.a20230301_melissaarellano_nycschools.domain.models.SchoolScores
import com.example.a20230301_melissaarellano_nycschools.domain.repository.SchoolRepository
import com.example.a20230301_melissaarellano_nycschools.getOrAwaitValue
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailsViewModelTest {

    private val repository: SchoolRepository = mockk(relaxed = false)
    private val viewModel: DetailsViewModel = DetailsViewModel(repository)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `When loadSatScores returns scores then it is posted in schoolScores`() {
        val school = School(
            dbn = "1234"
        )
        val satScore: SchoolScores = mockk(relaxed = true)
        //GIVEN
        coEvery { repository.getSchoolScores(school.dbn) } returns satScore

        //WHEN
        viewModel.loadSatScores(school)
        val state = viewModel.schoolScores.getOrAwaitValue()

        //THEN
        assertThat(state).isNotNull()
    }

    @Test
    fun `When loadSatScores is not found then null is posted in schoolScores`() {
        val school = School(
            dbn = "1234"
        )
        //GIVEN
        coEvery { repository.getSchoolScores(school.dbn) } returns null

        //WHEN
        viewModel.loadSatScores(school)
        val state = viewModel.schoolScores.getOrAwaitValue()

        //THEN
        assertThat(state).isNull()
    }
}